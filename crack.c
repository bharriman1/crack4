#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "md5.h"

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings


// Stucture to hold both a plaintext password and a hash.
struct entry 
{
    char password[PASS_LEN];
    char hash[HASH_LEN];
};

int file_length(char *filename)
{ 
    struct stat info;
    int ret = stat(filename, &info);
    if (ret == -1)
    {
        return -1;
    }
    else
    {
        return info.st_size;
    }

}

// Read in the dictionary file and return an array of structs.
struct entry *read_dictionary(char *filename, int *size)
{
    *size = 0;
    int len = file_length(filename);
    char *dict = malloc(len);
    
    //Read in the dictionary file
    FILE *f = fopen(filename, "r");
    if (!f)
    {
        printf("Can't open %s for reading", filename);
    }
    
    fread(dict, sizeof(char), len, f);
    fclose(f);
    
    //Parse the file to remove newlines and replace with null characters to create strings
    int num =0;
    
    for(int i=0; i<len; i++)
    {
        if (dict[i] == '\n')
        {
            dict[i] = '\0';
            num++;
        }
    }
    
    //Load up an array of structs
    struct entry *dictionary = malloc(num * sizeof(struct entry));
    
    //Load the structs into the array
    
    //Take pass out of array
    sscanf(&dict[0], "%s", dictionary[0].password);
    
    //Hash it
    char *hash = md5(dictionary[0].password, strlen(dictionary[0].password));
    
    //Copy it into the array at hash
    strcpy(dictionary[0].hash, hash);
    
    int j=1;
    for (int i=0; i<len-1; i++)
    {
        if (dict[i] == '\0')
        {
            sscanf(&dict[i+1], "%s", dictionary[j].password);
            char *hash = md5(dictionary[j].password, strlen(dictionary[j].password));
            strcpy(dictionary[j].hash, hash);
            j++;
        }
    }
    
    //How many structs are in the array
    *size = j;
    
    //Free up allocated memory
    
    
    //Return the array of structs
    return dictionary;
    
    
}

int compareHashes(const void *a, const void *b)
{
    return strcmp(((*(struct entry *)a).hash), ((*(struct entry *)b).hash));
}

int hashsearch(const void *target, const void *elem)
{
    return strcmp((char *)target, (*(struct entry *)elem).hash);
}

int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }
    
    //Read the dictionary file into an array of entry structures
    int dlen;
    struct entry *dict = read_dictionary(argv[2], &dlen);
    
    //Sort the hashed dictionary using qsort.
    qsort(dict, dlen, sizeof(struct entry), compareHashes);

    // Open the hash file for reading.
    FILE *fp = fopen(argv[1], "r");
    if (!fp)
    {
        printf("Can't open %s for reading", argv[1]);
        exit(2);
    }

    // For each hash, search for it in the dictionary using
    // binary search.
    // If you find it, get the corresponding plaintext dictionary word.
    // Print out both the hash and word.
    // Need only one loop. (Yay!)
    char line[100];
    
    while(fgets(line, 100, fp))
    {
        if (line[strlen(line)-1] == '\n')
        {
            line[strlen(line)-1] = '\0';
        }
        
        struct entry *found = bsearch(line, dict, dlen, sizeof(struct entry), hashsearch);
        
        if(found == NULL)
        {
            printf("%d", dlen);
        }
        else
        {
            printf("Hash: %s, Password: %s\n", found->hash, found->password);
        }
    }
    
}
